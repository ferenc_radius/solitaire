/**
 * Solitaire game.
 * 
 * @author Ferenc Radius - http://www.ferenc-radius.nl
 */
const TYPE_HEARTS   = 0x01;
const TYPE_SPADES   = 0x02;
const TYPE_CLUBS    = 0x04;
const TYPE_DIAMONDS = 0x08;
const TYPE_RED      = 0x10;
const TYPE_BLACK    = 0x20;

var CHARS = [];
CHARS[TYPE_HEARTS]   =  '♥';
CHARS[TYPE_SPADES]   =  '♠';
CHARS[TYPE_CLUBS]    =  '♣';
CHARS[TYPE_DIAMONDS] =  '♦';

var CHARD_LABELS = {
    1: 'A',
    2: '2',
    3: '3',
    4: '4',
    5: '5',
    6: '6',
    7: '7',
    8: '8',
    9: '9',
    10: '10',
    11: 'B',
    12: 'V',
    13: 'K'
};

var NODE_DISTANCE    = 20;
var NODE_SIZE        = 65;

var Solitaire = {

    columns:          new SolitaireTree(),
    targets:          new SolitaireTree(),
    pileNodes:        [],
    cardContainer:    document.getElementById("card-container"),
    targetContainer:  document.getElementById("target-container"),
    pileSource:       document.getElementById("pile-source"),
    pileShow:         document.getElementById("pile-show"),

    /**
     * Start Solitaire game.
     */
    start: function()
    {
        this.createPile();
        this.pileNodes.sort(this.randomComparator);
        this.createTarget();
        this.createColumns();
        this.createPileSource();
    },

    /**
     * Create pile source
     */
    createPileSource: function ()
    {
        this.pileNodes.forEach(function(node) {
            var cardElement = Solitaire.createCardElement(0, 0, node);

            node.setElement(cardElement);
            Solitaire.pileSource.appendChild(cardElement);

            SolitairePileNodeDerorator.call(node);
        });

        this.pileSource.addEventListener("click", function() {
            var nodeElements = Solitaire.pileShow.getElementsByTagName("div");

            console.log(Solitaire.pileSource.getElementsByTagName("div").length);

            if( Solitaire.pileSource.getElementsByTagName("div").length === 0 )
            {
                for( var i = 0, j = nodeElements.length; i < j; i++ )
                {
                    var element = Solitaire.pileShow.removeChild( nodeElements[i] );
                    removeClass(element, "active");
                    Solitaire.pileSource.appendChild(element);

                }
            }
        }, false);
    },

    /**
     * Create colums and add nodes to the colums SolitaireTree.
     */
    createColumns: function()
    {
        for( var i = 0; i < 7; i++ )
        {
            for( var x = 0; x < i+1; x++ )
            {
                if( x === 0 )
                {
                    var emptyNode = new SolitaireNode(0, 0);
                    this.addCardElementToNode(i, x, emptyNode, this.createCardElement(i, x, emptyNode), true);
                }

                var node = this.pileNodes.pop();
                this.addCardElementToNode(i, x, node, this.createCardElement(i, x, node));
            }
        }
    },

    /**
     * Create card element
     *
     * @param column
     * @param row
     * @param node
     */
    createCardElement: function(column, row, node)
    {
        var cardElement = document.createElement("div");
        var pointX      = (column * (NODE_SIZE + NODE_DISTANCE));
        var pointY      = (row * (NODE_SIZE - NODE_DISTANCE));
        var type        = CHARS[node.getNodeType() ^ TYPE_BLACK] || CHARS[node.getNodeType() ^ TYPE_RED];

        if( type )
        {
            cardElement.innerHTML    = '<span>' + type + " " + CHARD_LABELS[node.getNodeValue()] + '</span>';
        }

        cardElement.className    = "card";
        cardElement.style.width  = NODE_SIZE + "px";
        cardElement.style.height = NODE_SIZE + "px";
        cardElement.style.left   = pointX + "px";
        cardElement.style.top    = pointY + "px";

        // add some color to it
        if( node.getNodeType() & TYPE_HEARTS || node.getNodeType() & TYPE_DIAMONDS )
        {
            addClass(cardElement, "red");
        }

        return cardElement;
    },

    /**
     * Fill node
     *
     * @param column
     * @param row
     * @param node
     * @param cardElement
     * @param emptyNode
     */
    addCardElementToNode: function(column, row, node, cardElement, emptyNode)
    {
        var pointX      = (column * (NODE_SIZE + NODE_DISTANCE));
        var pointY      = (row * (NODE_SIZE - NODE_DISTANCE));

        // associate card with
        node.setElement(cardElement);

        // associate column index
        node.setColumn(column);

        // decorate as an empty node ?
        if( emptyNode )
        {
            SolitaireEmptyNodeDecorator.call(node);
            SolitaireCardNodeDecorator.call(node, pointX, pointY - NODE_DISTANCE);
        }
        else
        {
            // row same as column number then set the card active
            if( row  === column)
            {
                addClass(cardElement, "active");

                node = SolitaireCardNodeDecorator.call(node, pointX, pointY);

                this.columns.addNode(column, node);
            }

            // otherwise make the card appear when you click it.
            else
            {
                SolitaireHiddenColumnNodeDecorator.call(node, pointX, pointY);
            }
        }

        this.cardContainer.appendChild(cardElement);
    },

    /**
     * Get (remove from container) card element. 
     *
     * @param node
     * @param dragSource
     *
     * @returns DomElement
     */
    getElementByDragSource: function(node, dragSource)
    {
        var element = null;

        if( dragSource === 0 )
        {
            element = Solitaire.cardContainer.removeChild( node.getElement() );
        }

        else if( dragSource === 1 )
        {
            // move card element to cardContainer
            element = Solitaire.targetContainer.removeChild( node.getElement() );
        }

        // dragged from pile
        else if( dragSource === 2 )
        {
            element = Solitaire.pileShow.removeChild(node.getElement());
        }

        return element;
    },

    /**
     * Find node by index 
     *
     * @param index
     */
    getNodeByIndex: function(index)
    {
        var node = Solitaire.columns.getNodeByIndex(index);

        if( node === null )
        {
            // Maybe it is a card from the targets
            node = Solitaire.targets.getNodeByIndex(index);

            // no card on the targets so maybe it is from the pile
            if( node === null )
            {
                for( var i = 0, j = Solitaire.pileNodes.length; i < j && node === null; i++ )
                {
                    if( Solitaire.pileNodes[i].getIndex() === index )
                    {
                        node = Solitaire.pileNodes[i];
                    }
                }
            }

            // whatever happened it is time to stop.
            if( node === null )
            {
                return false
            }
        }

        return node;
    },

    /**
     * Remove node from parent (or column)
     * 
     * @param node
     * @param dragSource
     */
    removeFromParent: function(node, dragSource)
    {
        // remove from previous column ( when it is the root node )
        if( dragSource === 0 )
        {
            Solitaire.columns.remove(node);
        }
        else if( dragSource === 1 )
        {
            Solitaire.targets.remove(node);
        }
        else if( dragSource === 2 )
        {
            for( var i = 0, j = this.pileNodes.length; i < j; i++ )
            {
                console.log(this.pileNodes[i]);
                if( this.pileNodes[i].getIndex() === node.getIndex() )
                {
                    //this.pileNodes.splice(i, 1);
                }
            }
        }
    },

    /**
     * Create target columns
     */
    createTarget: function ()
    {
        for( var i = 0; i < 4; i++ )
        {
            var target = document.createElement("div");
            var pointX = (i * (NODE_SIZE + NODE_DISTANCE));

            target.className    = "target";
            target.style.width  = NODE_SIZE + "px";
            target.style.height = NODE_SIZE + "px";
            target.style.left   = pointX + "px";

            var targetNode = new SolitaireNode(0, 0);

            targetNode.setColumn(i);
            targetNode.setElement(target);

            SolitaireTargetNodeDecorator.call(targetNode, pointX);
            
            this.targetContainer.appendChild(target);
        }
    },

    /**
     * create pile (52 cards)
     */
    createPile: function()
    {
        this.pileNodes = [];

        for( var i = 0; i < 13; i++ )
        {
            var node = new SolitaireNode(i+1, TYPE_HEARTS + TYPE_RED);
            node.setIndex(this.pileNodes.length);
            this.pileNodes.push(node);

            node = new SolitaireNode(i+1, TYPE_SPADES + TYPE_BLACK);
            node.setIndex(this.pileNodes.length);
            this.pileNodes.push(node);

            node = new SolitaireNode(i+1, TYPE_CLUBS + TYPE_BLACK);
            node.setIndex(this.pileNodes.length);
            this.pileNodes.push(node);

            node = new SolitaireNode(i+1, TYPE_DIAMONDS + TYPE_RED);
            node.setIndex(this.pileNodes.length);
            this.pileNodes.push(node);
        }
    },

    /**
     * Comparator to shuffle the pile array.
     */
    randomComparator: function()
    {
        return 0.5 - Math.random()
    }
};

Solitaire.start();
