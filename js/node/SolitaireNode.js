var SolitaireNode = function(value, type)
{
    var nodeValue   = value;
    var nodeType    = type;
    var parent      = null;
    var child       = null;
    var domElement  = null;
    var column      = 0;
    var index;
    var events      = [];
    var dragSource  = 0;
    var pointX      = 0;
    var pointY      = 0;

     /**
     * x coordinate associated with the card dom element
     *
     * @returns int
     */
    this.getPointX = function()
    {
        return pointX;
    };

    /**
     * y coordinate associated with the card dom element
     *
     * @returns int
     */
    this.getPointY = function()
    {
        return pointY;
    };

     /**
     * x coordinate associated with the card dom element
     *
     */
    this.setPointX = function(x)
    {
        pointX = x;
    };

    /**
     * y coordinate associated with the card dom element
     *
     */
    this.setPointY = function(y)
    {
        pointY = y;
    };

    /**
     * default dragSource is from column to column
     *
     * 0 - column to column
     * 1 - target to column
     * 2 - pile to x
     *
     */
    this.getDragSource = function()
    {
        return dragSource;
    };

    /**
     * Set dragsource
     *
     * 0 - column to column
     * 1 - target to column
     * 2 - pile to x
     *
     * @param i
     */
    this.setDragSource = function(i) 
    {
        dragSource = i;
    };

    /**
     * Get registered events
     */
    this.getEvents = function()
    {
        return events;
    };

    /**
     * Add event to node element
     *
     * @param eventType
     * @param listener
     */
    this.addEvent = function(eventType, listener)
    {
        if(this.getElement() !== null)
        {
            this.getElement().addEventListener(eventType, listener, false);
            events.push({eventType: eventType, listener: listener});
        }
    };

    /**
     * Remove all events from node element
     */
    this.removeEventsByType = function(eventType)
    {
        eventType = eventType || null;

        for( var i = 0, j = events.length; i < j; i++ )
        {
            if( eventType === events[i].eventType )
            {
                this.getElement().removeEventListener(events[i].eventType, events[i].listener, false);
            }
        }
    };

    /**
     * Set parent from this node.
     *
     * @param node SolitaireNode
     */
    this.setParent = function(node)
    {
        // update column
        this.setColumn( node.getColumn() );
        parent = node;

        return node;
    };

    /**
     * Set child on this node.
     *
     * @param node
     * @returns Boolean
     */
    this.setChild = function(node)
    {
        // set this node as parent
        node.setParent(this);
        child = node;

        // return altered node
        return node;
    };

    /**
     * Add domElement
     *
     * @param element
     */
    this.setElement = function(element)
    {
        domElement = element;
    };

     /**
     * Get domElement
     *
     * @returns DomElement
     */
    this.getElement = function()
    {
        return domElement;
    };

    /**
     * Index associated with the card
     *
     * @param i
     */
    this.setIndex = function(i)
    {
        index = i;
    };

    /**
     * Index associated with the card
     *
     * @returns int
     */
    this.getIndex = function()
    {
        return index;
    };

    /**
     * Returns node type (bitwise combination of card type and card color)
     *
     * @returns int
     */
    this.getNodeType = function()
    {
        return nodeType;
    };

    /**
     * returns the node value
     *
     * @returns int;
     */
    this.getNodeValue = function()
    {
        return nodeValue;
    };

    /**
     * Retrieve the parent node
     *
     * @return SolitaireNode
     */
    this.getParent = function()
    {
        return parent;
    };

    /**
     * Retrieve the child node
     *
     * @return SolitaireNode
     */
    this.getChild = function()
    {
        return child;
    };

    /**
     * Remove child from parent, can be used to move the child
     * to a different node.
     *
     * @return SolitaireNode
     */
    this.removeChild = function()
    {
        var tmp = child;
        child = null;
        return tmp;
    };

    /**
     * Set column of card
     * @param i
     */
    this.setColumn = function(i)
    {
        column = i;
    };

    /**
     * Get column of card.
     *
     * @returns int
     */
    this.getColumn = function()
    {
        return column;
    };

    /**
     * Make node draggable
     *
     * @param drop Function - event callback
     */
    this.makeDraggable = function(self, drop)
    {
        if( this.getElement() === null )
        {
            return;
        }

        // clean up
        this.removeEventsByType("dragstart");
        this.removeEventsByType("drag");
        this.removeEventsByType("dragend");
        this.removeEventsByType("dragenter");
        this.removeEventsByType("dragover");
        this.removeEventsByType("drop");


        // make card draggable
        this.getElement().setAttribute("draggable", "true");

        this.addEvent("dragstart", function(event) {
            event.dataTransfer.setData('Text', self.getIndex());
            event.dataTransfer.effectAllowed = "copy";
        });

        this.addEvent("drag", function() {
            addClass(self.getElement(), "moving");
        });

        this.addEvent("dragend", function() {
            setTimeout(function() {
                removeClass(self.getElement(), "moving");
            }, 350);
        });

        this.addEvent("dragenter", function(event) {
            event.preventDefault();
            return false;
        });

        this.addEvent("dragover", function(event) {
            event.preventDefault();
            return false;
        });

        if( drop )
        {
            this.addEvent("drop", function(event) {
                event.preventDefault();

                var index = parseInt(event.dataTransfer.getData('Text'), 10);
                var dragNode = Solitaire.getNodeByIndex(index);

                // dropped on itself no action or no dragNode found
                if( ! dragNode || self.getIndex() === dragNode.getIndex() )
                {
                    return false;
                }

                // stop 'moving' the card
                removeClass(dragNode.getElement(), "moving");

                // save dragSource before it is changed by the decorator.
                var dragSource = dragNode.getDragSource();

                drop.call(this, event, index, dragNode, dragSource);
            });
        }
    }
};
