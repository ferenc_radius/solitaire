var SolitaireTree = function()
{
    var children = [];

    /**
     * Add node to column (and create column if not exists)
     *
     * @param index
     * @param node
     *
     * @returns Boolean when the child is valid and successfully added to the tree.
     */
    this.addNode = function(index, node)
    {
        if( node.setColumn )
        {
            node.setColumn(index);
        }

        if ( !children[index] )
        {
            children[index] = node;
        }
        else
        {
            node = children[index].setChild(node);
        }

        return node;
    };

    /**
     * Remove root node (only if node is a root node)
     *
     * @param node SolitaireNode
     */
    this.remove = function(node)
    {
        if( node.getParent() !== null )
        {
            node.getParent().removeChild();
        }
        else
        {
            this.removeByColumnIndex(node.getColumn());
        }
    };

    /**
     * Remove column by index
     *
     * @param index
     */
    this.removeByColumnIndex = function(index)
    {
        children[index] = null;
    };

    /**
     * Find node by the node index
     *
     * @param index
     */
    this.getNodeByIndex = function(index)
    {
        index = parseInt(index, 10);

        for( var i = 0, j = children.length; i < j; i++ )
        {
            // no node on row
            if( children[i] === null || children[i] === undefined )
            {
                continue;
            }

            // is a direct child ?
            if( children[i].getIndex() === index )
            {
                return children[i];
            }

            // walk through children to look if it is there.
            var child = children[i].getChild();

            while( child !== null )
            {
                if( child.getIndex() === index)
                {
                    return child;
                }

                child = child.getChild();
            }
        }

        return null;
    };

    /**
     * Get first node by the column number
     *
     * @param index
     */
    this.getByColumn = function(index)
    {
        return children[index];
    };

    /**
     * Retrieve children
     *
     * @return Array
     */
    this.getChildren = function()
    {
        return children;
    };
};