var SolitaireCardNodeDecorator = function(x, y)
{
    var self = this;

    this.setPointX(x);
    this.setPointY(y);
    this.setDragSource( 0 );

    /**
     * set the position of the node.
     * @param x
     * @param y
     */
    this.setPosition = function(x, y)
    {
        self.setPointX(x);
        self.setPointY(y);

        self.getElement().style.left = x;
        self.getElement().style.top = y;

        // fix z order
        if( self.getParent() !== null )
        {
            self.getElement().style.zIndex = self.getParent().getElement().style.zIndex + 1;
        }

        var child = self.getChild();
        while( child !== null && child.setPosition )
        {
            child.setPosition(x, y + NODE_DISTANCE);
            child = child.getChild();
        }
    };

    /**
     * Is the child valid to add to the parent ?
     *
     * @param parent
     * @param child
     */
    this.validChild = function(parent, child)
    {
        // the parent node value should be exactly be 1 more then the value of its child
        if( parent !== null && parent.getNodeValue() - child.getNodeValue() !== 1 )
        {
            return false;
        }

        // when there is no parent the card child should be a king (13)
        else if( parent === null && child.getNodeValue() !== 13 )
        {
            return false;
        }

        // nodes should not be the same color.
        return (parent === null || ((parent.getNodeType() & TYPE_BLACK) - (child.getNodeType() & TYPE_BLACK)) !== 0);
    };

    // remove click event from pile card
    this.removeEventsByType("click");

    if( this.emptyNode )
    {
        addClass(this.getElement(), "empty-node");
    }

    this.removeEventsByType("drop");

    // make draggable
    this.makeDraggable(this, function(event, index, dragNode, dragSource) {

        // move to empty node ( so only allowed, when card is a king (13) )
        var parent = self;

        if( self.emptyNode )
        {
            parent = null;
        }

        if ( self.validChild(parent, dragNode) )
        {
            // make it a card node (so card from target/pile are corrected)
            SolitaireCardNodeDecorator.call(dragNode, self.getPointX(), self.getPointY());

            Solitaire.removeFromParent( dragNode, dragSource );

            // add node to the column
            dragNode = Solitaire.columns.addNode(self.getColumn(), dragNode);

            var element = Solitaire.getElementByDragSource(dragNode, dragSource);
            
            if ( element !== null )
            {
                Solitaire.cardContainer.appendChild(element);
            }

            // set position of the node.
            dragNode.setPosition(self.getPointX(), self.getPointY() + NODE_DISTANCE);
        }

        return false;
    });

    return this;
};