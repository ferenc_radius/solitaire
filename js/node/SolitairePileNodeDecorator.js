var SolitairePileNodeDerorator = function()
{
    this.setDragSource(2);
    var self = this;

    if( this.getElement() !== null )
    {
        // TODO add real card (show next 3 etc)
        this.addEvent("click", function() {
            
            var element = Solitaire.pileSource.removeChild( self.getElement() );
            Solitaire.pileShow.appendChild(element);

            addClass(self.getElement(), "active");

            self.makeDraggable(self);
        });
    }

    return this;
};
