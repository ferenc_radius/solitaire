var SolitaireHiddenColumnNodeDecorator = function(x, y)
{
    var self = this;

    this.setPointX(x);
    this.setPointY(y);

    // this will turn the card arround
    this.getElement().addEventListener("click", function(event) {
        event.preventDefault();

        // valid to turn arround ?
        if( Solitaire.columns.getByColumn(self.getColumn()) === null )
        {
            var node = SolitaireCardNodeDecorator.call(self, self.getPointX(), self.getPointY());
            addClass(self.getElement(), "active");
            node = Solitaire.columns.addNode(node.getColumn(), node);
        }

        return false;
    }, true);

    return this;
};
