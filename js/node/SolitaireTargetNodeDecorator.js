var SolitaireTargetNodeDecorator = function(x)
{
    this.setDragSource(1);
    this.setPointX(x);

    var self = this;

    // the child ( node to add to the target) should not have any children.
    this.validChild = function(parent, child)
    {
        // no parent so the card should be a ace (1)
        if( parent === null && child.getNodeValue() !== 1)
        {
            return false;
        }

        // only leafs are allowed
        else if( child.getChild() !== null )
        {
            return false;
        }

        // child should be the same type as the as the parent
        else if( parent !== null && parent.getNodeType() !== child.getNodeType() )
        {
            return false;
        }

        // the child node value should be exactly be 1 more then the value of its parent
        else if( parent !== null  && child.getNodeValue() - parent.getNodeValue() !== 1)
        {
            return false;
        }

        return true;
    };

    this.makeDraggable(this, function(event, index, dragNode, dragSource) {

        // When parent is set it should be the target parent and not the parent from the dragNode
        var parent = self.getNodeValue() > 0 ? self : null;

        if( self.validChild(parent, dragNode) )
        {
            Solitaire.removeFromParent(dragNode, dragSource);

            var element = Solitaire.getElementByDragSource(dragNode, dragSource);

            // dragged from pile
            if ( dragSource == 2 )
            {
                SolitaireCardNodeDecorator.call(dragNode, self.getPointX(), self.getPointY());
            }

            // make the card a target card
            SolitaireTargetNodeDecorator.call(dragNode, self.getPointX());

            if( element !== null )
            {
                // move element to target container
                Solitaire.targetContainer.appendChild(element);

                // set position
                dragNode.setPosition(self.getPointX(), 1);
            }

            // add to target tree.
            Solitaire.targets.addNode(self.getColumn(), dragNode);

            //TODO check if game is done ?
        }

        return false;
    });

    return this;
};